# Introduction #

VetSens designs products that monitor or measure animal health (the “Devices”), operates www.vetsens.co.uk (individually the “Website,” and collectively with its sub-domains, the “Websites”), the VetSens mobile applications (each, an “App,” and collectively, the “Apps”), and the VetSens API, Manuals and Developer Resources (the “Resources”). The Devices, Websites, Apps, and Resources are collectively referred to as the VetSens “Service” or VetSens “Platform.”

The objective of the Resources is to foster innovation in animal health, extending and augmenting the VetSens user experience, and supporting complementary services in a collaborative animal health ecosystem. Applications and developers accessing and using the VetSens Resources are required to comply with these policies as well as VetSens’ Terms of Use and Privacy Policy.

# Definitions #

**“Developer Application”** means any software application developed by you that interacts with the VetSens Platform

**“Customer Data”** means any data collected from Users (as defined below) of your Developer Application including, without limitation, the names, email addresses, and user profile pictures of any VetSens customer, as well as their animals’ names, profile pictures, gender, date of birth, breed, weight, location, medical conditions, and goals

**“VetSens Data”** means the dog health & lifestyle data collected by the VetSens Devices and made available to you through the VetSens Resources

**“User”** means a registered user of your Developer Application

**“VetSens User Data”** means that subset of the VetSens Data that is associated with a User

# Scope of the VetSens Resources #

You may use the VetSens Resources to create Developer Applications that interact with and extend and/or enhance the VetSens Platform or for data backup purposes

You may use the VetSens Resources to retrieve or post VetSens Data and Customer Data and display information in external applications according to the Terms

You may use the VetSens Resources to post select data from your application into the VetSens Platform, provided that you have express User consent

# Restrictions #

You may not use the VetSens Resources to design anything other than Developer Applications. You may make no further use of the VetSens data, without express User consent

You may not use the VetSens Resources to create Developer Applications that are intended to or do replace the VetSens Platform or direct VetSens users away from the VetSens Platform

You may not perform or encourage wholesale export of VetSens Data for the purpose of account migration or service duplication

You may not use the VetSens Resources for purposes of publishing known bad data into the VetSens Platform

You may not use the VetSens Resources to modify or reverse engineer any functionalities of the VetSens Platform or to access or attempt to access VetSens’s servers for purposes unrelated to the implementation of your Developer Application or to circumvent or attempt to circumvent the intended use of the VetSens Resources

You may not use the VetSens Resources to retrieve solicit or retrieve user passwords

You may not use web scraping, web harvesting, or web data extraction methods to extract data from the VetSens Platform or other VetSens web assets

You may not distribute, sell, lease, rent, lend, or sub-license any part of the VetSens Platform or related services or content to any third party except as included within and necessary to distribute your Developer Application

You may not use or access the VetSens Resources for the purpose of monitoring the performance or functionality of the VetSens Platform or for any other benchmarking or competitive purposes

We may monitor your usage of the VetSens Resources in order to improve our service and to ensure compliance with our policies. Use of the VetSens Resources is subject to reasonable restrictions on rate limit, as determined by us in our sole discretion. If we reasonably believe that you have exceeded such limits, your ability to utilise the VetSens Resources may be temporarily slowed, suspended, or permanently revoked, as we determine in our sole discretion

No unlawful activity

You won’t upload or transmit content that is unlawful, threatening, abusive, defamatory, vulgar, hateful, or racially ethically or otherwise objectionable

You won’t upload or transmit content that you do not have a right to transmit or that infringes any patent, trademark, trade secret, copyright, or any other proprietary rights

You won’t upload or transmit content that that maligns or misrepresents VetSens or that uses VetSens’ trademarks, copyrights, or other intellectual property outside the scope of the rights granted herein

You won’t harm minors

You won’t impersonate or otherwise misrepresent your affiliation with any person or entity

You won’t violate any applicable law or regulation including, without limitation, laws regarding the export of technical data

# Data attribution #

Any rendering or display of VetSens Data in your Developer Application must provide attribution to VetSens

# Licensing and IP ownership #

Subject at all times to your full compliance with the Terms, as well as VetSens’ Terms of Use and Privacy Policy, VetSens grants you a non-exclusive, revocable, non-sublicenseable, non-transferable royalty free license to use the VetSens Resources solely to develop, reproduce and distribute Developer Applications. VetSens has no obligation to provide any type of support for the VetSens Resources or any services or content related thereto

The VetSens Resources is licensed (and not sold) to you. The Terms in no way convey any ownership rights to you, and VetSens reserves all rights not expressly granted to you under the Terms

You are responsible for providing customer with technical support and maintenance for your Developer Application

You agree to not market, sell, transfer or disclose any Customer Data to any third parties, except as expressly permitted by the customer or except as otherwise provided by law. Users must be permitted to express contact preferences, via notice and opt-out, at the point of collection and in each subsequent marketing piece

The license granted herein implies no partnership or affiliation with VetSens, and your Developer Application content must not express or imply an affiliation or relationship with VetSens, unless otherwise provided for in a separate written and duly executed agreement

You may not promote or publicise your integration with the VetSens Resources until authorised to do so by VetSens

You may not use the VetSens name or related trademarks in any of your Developer Application names or URLs

VetSens reserves the right to disable or upgrade the VetSens Resources and related services at any time without notice to you and without any form of compensation or consideration to you, regardless of the status of any Developer Application

You hereby grant to VetSens a worldwide, non-exclusive, royalty free, license to copy, display, perform, transmit, and use any Developer Application and related trademarks and logos that you create using the VetSens Resources, for the purposes of promoting VetSens and marketing your Developer Applications to customers. However, VetSens has no obligation to promote any Developer Application.

# Warranty #

You represent and warrant that you have all rights, including all copyright, trademark and other intellectual property rights, in the Developer Application necessary to offer your Developer Application to end users and to grant the license to VetSens in this Agreement

# Feedback #

Any feedback, suggestions and ideas that you provide to us regarding the VetSens Resources, the VetSens Platform, or any other VetSens product or service will be treated by us as non-confidential, and we may, in our sole discretion, use the feedback you provide in any way, including future modifications of the VetSens Resources, services, products, multimedia works and/or advertising and promotional materials relating thereto

You hereby grant VetSens a perpetual, worldwide, fully transferable, irrevocable, royalty free license to use, reproduce, modify, create derivative works from, distribute, and display your feedback in any manner and for any purpose

# Modification #

We may change the Terms or any other terms of the VetSens Platform at any time, without prior notice, as we deem necessary in our sole discretion

Your continued use of the VetSens Resources constitutes acceptance of those changes.
If the changes are unacceptable to you, your only recourse is to immediately discontinue your use of the VetSens Resources

# Termination #

VetSens may change, suspend, or discontinue all or any aspect of the VetSens Resources, including its availability, at any time, and may suspend or terminate your use of the VetSens Resources at any time. VetSens may terminate your access to the VetSens Resources and your right to use it at any time, for any reason, or for no reason at all

Upon any termination of this agreement, you will promptly cease using the VetSens Resources and the VetSens Data. You may also terminate use of the VetSens Resources at any time.

Following termination for any reason by either party, you are required to delete all data you receive or have received through the VetSens Resources

# Confidentiality #

You may be given access to certain non-public, proprietary information, software, and specifications related to the VetSens Resources or the VetSens Platform

You may use this confidential information only as necessary in exercising the rights granted to you by the Terms

You may not disclose any confidential information to any third party without our prior written consent and you agree that you will protect this confidential information from unauthorised use, access, or disclosure in the same manner that you would use to protect your own confidential and proprietary information of a similar nature and in any event with no less than a reasonable degree of care

# Disclaimer of warranties #

YOUR USE OF THE VETSENS RESOURCES IS AT YOUR SOLE RISK. THE VETSENS RESOURCES ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS. VETSENS EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS, IMPLIED, STATUTORY, OR OTHERWISE, INCLUDING ANY WARRANTY OF MERCHANTABILITY, NON-INFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE, AND ANY WARRANTIES OR CONDITIONS ARISING OUT OF COURSE OF DEALING OR USAGE OF TRADE

VETSENS MAKES NO WARRANTY THAT THE VETSENS RESOURCES WILL MEET YOUR REQUIREMENTS, THAT IT WILL BE UNINTERRUPTED, TIMELY, SECURE, VIRUS-FREE OR ERROR-FREE, OR THAT ANY ERRORS IN THE SOFTWARE WILL BE CORRECTED

ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE API IS RETRIEVED AT YOUR OWN DISCRETION AND RISK AND. YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM/NETWORK OR ANY LOSS OF DATA THAT MAY RESULT FROM THE DOWNLOAD OF ANY SUCH MATERIAL OR THE USE OF THE API

# Liability #

IN NO EVENT WILL VETSENS BE LIABLE FOR ANY SPECIAL, INCIDENTAL, EXEMPLARY, PUNITIVE, OR CONSEQUENTIAL DAMAGES (INCLUDING, WITHOUT LIMITATION, LOSS OF USE, DATA, BUSINESS, OR PROFITS) ARISING FROM OR IN CONNECTION WITH THE TERMS OR YOUR USE OF THE VETSENS RESOURCES OR VETSENS PLATFORM, WHETHER ARISING FROM ANY CLAIM BASED UPON CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY, OR OTHERWISE, AND REGARDLESS OF WHETHER OR NOT YOU HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS OR DAMAGE

THE FOREGOING LIMITATIONS SHALL SURVIVE AND APPLY EVEN IF ANY LIMITED REMEDY SPECIFIED IN THE TERMS IS FOUND TO HAVE FAILED ITS ESSENTIAL PURPOSE

CERTAIN JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES. ACCORDINGLY, SOME OF THE ABOVE LIMITATIONS MAY NOT APPLY TO YOU

# Indemnification #

YOU AGREE TO INDEMNIFY AND HOLD HARMLESS VETSENS AND ITS SUBSIDIARIES, AFFILIATES, OFFICERS, AGENTS, AND EMPLOYEES, ADVERTISERS, LICENSORS, AND PARTNERS, FROM AND AGAINST ANY THIRD PARTY CLAIM ARISING FROM OR IN ANY WAY RELATED TO YOUR USE OF THE VETSENS RESOURCES, IMPLEMENTATION OF YOUR DEVELOPER APPLICATION OR VIOLATION OR BREACH BY YOU OF THE TERMS OR ANY OTHER POLICIES OR TERMS AND CONDITIONS RELATING TO THE VETSENS PLATFORM, OR INFRINGEMENT OF THE INTELLECTUAL PROPERTY RIGHTS OF ANY THIRD PARTY, INCLUDING ANY LIABILITY OR EXPENSE ARISING FROM ALL CLAIMS, LOSSES, DAMAGES (ACTUAL AND CONSEQUENTIAL), SUITS, JUDGMENTS, LITIGATION COSTS, AND ATTORNEYS’ FEES, OF EVERY KIND AND NATURE
YOU AGREE TO USE COMMERCIALLY REASONABLE MEASURES TO MAINTAIN THE SECURITY OF CUSTOMER DATA COLLECTED IN CONNECTION WITH ANY OF YOUR PRODUCTS OR SERVICES OFFERED, AND YOU WILL BE SOLELY RESPONSIBLE FOR, AND WILL INDEMNIFY US FOR, ANY LOSS, HARM, OR LIABILITY RELATED TO YOUR FAILURE TO ADEQUATELY PROTECT CUSTOMER DATA

# Entire agreement #

The Terms constitute the entire agreement among the parties with respect to the subject matter hereof and supersede all prior proposals, understandings and communications between the parties with respect to that subject matter

If this document and the Terms conflict in any way, the provisions in the Terms supersede this document

# Assignment #

If you are acquired by or merge with a third party, you may continue to use VetSens Data or data generated therefrom within your Developer Application only if you obtain express consent from users from whom the data was retrieved

# Equitable remedies #

You acknowledge that your violation or breach of the Terms may cause irreparable harm to VetSens. Accordingly, you agree that, in addition to any other remedies to which VetSens may be legally entitled, VetSens shall have the right to seek immediate injunctive relief in the event of such violation or breach by you or any of your officers, employees, consultants or other agents.

# Jurisdiction #

The Terms shall be governed and construed in accordance with the laws of England and Wales.
All claims arising out of or relating to the Terms will be brought exclusively to the courts of England and Wales and you consent to personal jurisdiction in those courts.

# Severability and waiver #

No waiver by VetSens of any covenant or right under the Terms will be effective unless given in a writing duly authorised by VetSens.

If any part of the Terms is determined to be invalid or unenforceable by a court of competent jurisdiction, that provision will be enforced to the maximum extent permissible and the remaining provisions of the Terms will remain in full force and effect